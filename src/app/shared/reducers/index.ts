import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'src/app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'src/app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'src/app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'src/app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'src/app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'src/app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'src/app/modules/account/password-reset/password-reset.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
});

export default rootReducer;
